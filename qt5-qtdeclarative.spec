%global __provides_exclude_from ^%{_qt5_archdatadir}/qml/.*\\.so$
%define _lto_cflags %{nil}
%global qt_module qtdeclarative
%global multilib_archs x86_64 %{ix86} %{?mips} ppc64 ppc s390x s390 sparc64 sparcv9

Name:             qt5-%{qt_module}
Version:          5.15.10
Release:          1
Summary:          Qt5 - QtDeclarative component
License:          LGPL-3.0-only OR GPL-3.0-only WITH Qt-GPL-exception-1.0
Url:              http://www.qt.io
%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:          https://download.qt.io/official_releases/qt/%{majmin}/%{version}/submodules/%{qt_module}-everywhere-opensource-src-%{version}.tar.xz
Source5:          qv4global_p-multilib.h

Patch1:           0001-Remove-unused-QPointer-QQuickPointerMask.patch
Patch2:           0002-QQmlDelegateModel-Refresh-the-view-when-a-column-is-.patch
Patch3:           0003-Fix-TapHandler-so-that-it-actually-registers-a-tap.patch
Patch4:           0004-Revert-Fix-TapHandler-so-that-it-actually-registers-.patch
Patch5:           0005-Make-sure-QQuickWidget-and-its-offscreen-window-s-sc.patch
Patch6:           0006-QQuickItem-Guard-against-cycles-in-nextPrevItemInTab.patch
Patch7:           0007-Don-t-convert-QByteArray-in-startDrag.patch
Patch8:           0008-Fix-build-after-95290f66b806a307b8da1f72f8fc2c698019.patch
Patch9:           0009-Implement-accessibility-for-QQuickWidget.patch
Patch10:          0010-Send-ObjectShow-event-for-visible-components-after-i.patch
Patch11:          0011-QQuickItem-avoid-emitting-signals-during-destruction.patch
Patch12:          0012-a11y-track-item-enabled-state.patch
Patch13:          0013-Make-QaccessibleQuickWidget-private-API.patch
Patch14:          0014-Qml-Don-t-crash-when-as-casting-to-type-with-errors.patch
Patch15:          0015-Fix-missing-glyphs-when-using-NativeRendering.patch
Patch16:          0016-Revert-Fix-missing-glyphs-when-using-NativeRendering.patch
Patch17:          0017-QQmlImportDatabase-Make-sure-the-newly-added-import-.patch
Patch18:          0018-QQuickState-when-handle-QJSValue-properties-correctl.patch
Patch19:          0019-Models-Avoid-crashes-when-deleting-cache-items.patch
Patch20:          0020-qv4function-Fix-crash-due-to-reference-being-invalid.patch
Patch21:          0021-Quick-Animations-Fix-crash.patch
Patch22:          0022-Prevent-crash-when-destroying-asynchronous-Loader.patch
Patch23:          0023-QQuickItem-Fix-effective-visibility-for-items-withou.patch
Patch24:          0024-Revert-QQuickItem-Fix-effective-visibility-for-items.patch
Patch25:          0025-Accessibility-respect-value-in-attached-Accessible-i.patch
Patch26:          0026-qml-tool-Use-QCommandLineParser-process-rather-than-.patch


Patch100:          %{name}-gcc11.patch
Patch101:          qtdeclarative-5.15.0-FixMaxXMaxYExtent.patch
Patch102:          qt-QTBUG-111935-fix-V4-jit.patch

# filter qml provides

Obsoletes:         qt5-qtjsbackend < 5.2.0
Obsoletes:         qt5-qtdeclarative-render2d < 5.7.1-10

BuildRequires:     make
BuildRequires:     gcc-c++
BuildRequires:     qt5-rpm-macros
BuildRequires:     qt5-qtbase-devel >= %{version}
BuildRequires:     qt5-qtbase-private-devel
%{?_qt5:Requires: %{_qt5}%{?_isa} = %{_qt5_version}}
BuildRequires:     python%{python3_pkgversion}

%if 0%{?bootstrap}
Obsoletes:         %{name}-examples < %{version}-%{release}
%global no_examples CONFIG-=compile_examples
%endif

%if 0%{?tests}
BuildRequires:     dbus-x11
BuildRequires:     mesa-dri-drivers
BuildRequires:     time
BuildRequires:     xorg-x11-server-Xvfb
%endif

%description
%{summary}.

%package devel
Summary:           Development files for %{name}
Obsoletes:         qt5-qtjsbackend-devel < 5.2.0
Obsoletes:         qt5-qtdeclarative-render2d-devel < 5.7.1-10
Provides:          %{name}-private-devel = %{version}-%{release}
Requires:          %{name}%{?_isa} = %{version}-%{release}
Requires:          qt5-qtbase-devel%{?_isa}
%description devel
%{summary}.

%package static
Summary:           Static library files for %{name}
Requires:          %{name}-devel%{?_isa} = %{version}-%{release}
%description static
%{summary}.

%package examples
Summary:           Programming examples for %{name}
Requires:          %{name}%{?_isa} = %{version}-%{release}
%description examples
%{summary}.


%prep
%autosetup -n qtdeclarative-everywhere-src-%{version} -p1

%build
#HACK so calls to "python" get what we want
ln -s %{__python3} python
export PATH=`pwd`:$PATH

%qmake_qt5

%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

%ifarch %{multilib_archs}
# multilib: qv4global_p.h
  mv %{buildroot}%{_qt5_headerdir}/QtQml/%{version}/QtQml/private/qv4global_p.h \
     %{buildroot}%{_qt5_headerdir}/QtQml/%{version}/QtQml/private/qv4global_p-%{__isa_bits}.h
  install -p -m644 -D %{SOURCE5} %{buildroot}%{_qt5_headerdir}/QtQml/%{version}/QtQml/private/qv4global_p.h
%endif

# hardlink files to %{_bindir}, add -qt5 postfix to not conflict
mkdir %{buildroot}%{_bindir}
pushd %{buildroot}%{_qt5_bindir}
for i in * ; do
  case "${i}" in
    # qt4 conflicts
    qmlplugindump|qmlprofiler)
      ln -v  ${i} %{buildroot}%{_bindir}/${i}-qt5
      ln -sv ${i} ${i}-qt5
      ;;
    qml|qmlbundle|qmlmin|qmlscene)
      ln -v  ${i} %{buildroot}%{_bindir}/${i}
      ln -v  ${i} %{buildroot}%{_bindir}/${i}-qt5
      ln -sv ${i} ${i}-qt5
      ;;
    *)
      ln -v  ${i} %{buildroot}%{_bindir}/${i}
      ;;
  esac
done
popd

pushd %{buildroot}%{_qt5_libdir}
for prl_file in libQt5*.prl ; do
  sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" ${prl_file}
  rm -fv "$(basename ${prl_file} .prl).la"
  sed -i -e "/^QMAKE_PRL_LIBS/d" ${prl_file}
done
popd


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
export PATH=%{buildroot}%{_qt5_bindir}:$PATH
export LD_LIBRARY_PATH=%{buildroot}%{_qt5_libdir}
make sub-tests-all %{?_smp_mflags}
xvfb-run -a \
dbus-launch --exit-with-session \
time \
make check -k -C tests ||:
%endif


%ldconfig_scriptlets

%files
%license LICENSE.LGPL*
%{_qt5_libdir}/libQt5Qml.so.5*
%{_qt5_libdir}/libQt5QmlModels.so.5*
%{_qt5_libdir}/libQt5QmlWorkerScript.so.5*
%{_qt5_libdir}/libQt5Quick.so.5*
%{_qt5_libdir}/libQt5QuickWidgets.so.5*
%{_qt5_libdir}/libQt5QuickParticles.so.5*
%{_qt5_libdir}/libQt5QuickShapes.so.5*
%{_qt5_libdir}/libQt5QuickTest.so.5*
%{_qt5_plugindir}/qmltooling/
%{_qt5_archdatadir}/qml/

%files devel
%{_bindir}/qml*
%{_qt5_bindir}/qml*
%{_qt5_headerdir}/Qt*/
%{_qt5_libdir}/libQt5Qml.so
%{_qt5_libdir}/libQt5Qml.prl
%{_qt5_libdir}/libQt5QmlModels.so
%{_qt5_libdir}/libQt5QmlModels.prl
%{_qt5_libdir}/libQt5QmlWorkerScript.so
%{_qt5_libdir}/libQt5QmlWorkerScript.prl
%{_qt5_libdir}/libQt5Quick*.so
%{_qt5_libdir}/libQt5Quick*.prl
%dir %{_qt5_libdir}/cmake/Qt5Quick*/
%{_qt5_libdir}/cmake/Qt5*/Qt5*Config*.cmake
%{_qt5_libdir}/metatypes/qt5*_metatypes.json
%{_qt5_libdir}/pkgconfig/Qt5*.pc
%{_qt5_archdatadir}/mkspecs/modules/*.pri
%{_qt5_archdatadir}/mkspecs/features/*.prf
%dir %{_qt5_libdir}/cmake/Qt5Qml/
%{_qt5_libdir}/cmake/Qt5Qml/Qt5Qml_*Factory.cmake
%{_qt5_libdir}/cmake/Qt5QmlImportScanner/

%files static
%{_qt5_libdir}/libQt5QmlDevTools.a
%{_qt5_libdir}/libQt5QmlDevTools.prl
%{_qt5_libdir}/libQt5PacketProtocol.a
%{_qt5_libdir}/libQt5PacketProtocol.prl
%{_qt5_libdir}/libQt5QmlDebug.a
%{_qt5_libdir}/libQt5QmlDebug.prl

%if ! 0%{?no_examples:1}
%files examples
%{_qt5_examplesdir}/
%endif


%changelog
* Mon Aug 21 2023 huayadong <huayadong@kylinos.cn> - 5.15.10-1
- update to version 5.15.10-1

* Fri Jan 7 2022 peijiankang <peijiankang@kylinos.cn> - 5.15.2-2
- rm qt5-qtdeclarative-5.15.2-7.eln112.src.rpm

* Wed Oct 13 2021 peijiankang <peijiankang@kylinos.cn> - 5.15.2-1
- update to upstream version 5.15.2

* Tue Oct 27 2020 wangxiao <wangxiao65@huawei.com> - 5.11.1-7
- delete python2 buildrequires

* Mon Sep 14 2020  liuweibo <liuweibo10@huawei.com> - 5.11.1-6
- Fix Source0

* Sat Feb 22 2020 yanzhihua <yanzhihua4@huawei.com> - 5.11.1-5
- modify python buildrequire

* Thu Nov 07 2019 yanzhihua <yanzhihua4@huawei.com> - 5.11.1-4
- Package init

